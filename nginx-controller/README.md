# Como realizar a instalação do NGINX Controller no cluster GKE

A instalação é bem simplificada e utiliza o gerenciador de pacotes HELM para realizar o deploy da aplicação.

## Instalação

1. Add repositório HELM

```sh
helm repo add nginx-stable https://helm.nginx.com/stable
```

2. Criando o Namespace que será utilizado pelo nginx

```sh
kubectl create namespace ingress-nginx
```
3. Instalando o NGINX Controller

```sh
helm install nginx-control nginx-stable/nginx-ingress -n ingress-nginx
```

## Verificando IP

Após o deploy da aplicação vamos verificar qual IP nosso NGINX pegou com o comando

```sh
export NGINX_INGRESS_IP=$(kubectl -n ingress-nginx get service nginx-control-nginx-ingress -o json | jq -r '.status.loadBalancer.ingress[].ip')
```

Guarde essa informação pois necessitaremos dela!
