---
marp: true
theme: default
backgroundColor: #fff
paginate: true
color: white
backgroundImage: url('imagens/22589.jpg')
style: |
    section{
      font-family: Arial, sans-serif, monospace;
    }
---
<style scoped>
  h1 {
    font-size: 125%;
    list-style-type: circle;
    text-align: center;
    font-weight: 900;
    color: #40E0D0
  }
  p {
    font-size: 21pt;
    text-align: right;
    list-style-type: circle;
    font-weight: 500;
    color: #00FF7F
  }
</style>

![bg width:55% center](imagens/metrics.png)
#
#
#
#
#
#
#
#
#
#
#
#
# Kubernetes: Observabilidade com ferramentas OpenSource

---
<style scoped>
  li {
  list-style-type: disc;
  padding: 0;
  width: 70%;
  margin-left: -10px;
}
  h3 {
    font-size: 125%;
    list-style-type: circle;
    font-weight: 900;
    color: #40E0D0
  }
</style>

![bg left:32% 95%](imagens/IMG_20220608_120558.jpg)

###  Jeovany Batista da Silva

* Construtor na 4Linux
* Especialista em Observabilidade
* Formado em Segurança da Informação
* Gamer 🎮 
* Adoro café ☕

---
<style scoped>
  h3 {
    font-size: 35pt;
    text-align: center;
    list-style-type: circle;
    font-weight: 900;
    color: 	#DCDCDC
  }
  p {
    font-size: 20pt;
    text-align: center;
  }
  h6 {
   font-size: 30px;
   text-align: center;
  }
  img[alt~="center"]{
    display: block;
    margin: 0 auto;
  }
</style>
#
 ![width:480px center](imagens/gitlab.png)
  ### Baixe esta apresentação!

---

<style scoped>
  blockquote {
    font-size: 30pt;
    color: #00FFFF;
    text-align: center;
  }
  p {
    color: #7FFFD4;
    text-align: center;
  }
</style>
#
<!-- _paginate: false -->

>#### Assim como a tecnologia evolui, a comunicação se transforma, e juntos abrem as portas para um mundo conectado, onde distâncias são encurtadas e ideias fluem livremente.

*Tim Berners-Lee, inventor da World Wide Web.*

---
<style scoped>
  h1 {
    font-size: 35pt;
    color: #00FFFF;
    text-align: center;
  }
  li {
  list-style-type: disc;
  padding: 0;
  width: 100%;
  margin-left: -10px;
  }
  img[alt~="right"]{
    display: block;
    margin: 0 auto;
  }
</style>

# Observabilidade

*  Necessidades
*  Tecnologia e Processos 
*  Ferramentas OpenSource
    * Prometheus
    * Grafana
  
![bg right:50% w:500](imagens/observabilidade.png)

  
---
<style scoped>
  li {
  list-style-type: disc;
  padding: 0;
  width: 100%;
  margin-left: -10px;
  }
  h1 {
    font-size: 35pt;
    color: #00FFFF;
    text-align: center;
  }
</style>

# Prometheus

* TSDB - Time Series Data Base
* Coletas de métricas em modelo pull sobre HTTP
* PromQL
* Trabalha com qualquer arquitetura de serviço
* Configuração estática ou com serviços de descobertas para Targets

![bg right:55% w:930](imagens/architecture2.png)

---
<style scoped>
  li {
  font-size: 23pt;
  width: 80%;
  margin-left: -10x;
  }
  h1 {
    font-size: 35pt;
    color: #00FFFF;
    text-align: left;
  }
</style>

# INSTRUMENTING

* Lib Oficiais:
  * Go;
  * Java ou Scala;
  * Python;
  * Ruby;
  * Rust;
* Tericeira Parte;

![bg w:90% right](imagens/Illustration-cloud-original-1216x840.png)

---
<style scoped>
  p {
   font-size: 25px;
   text-align: center;
  }
  h1 {
    font-size: 35pt;
    color: #00FFFF;
    text-align: center;
  }
</style>

# Grafana
O Grafana permite consultar, visualizar, alertar e explorar suas métricas, logs e rastreamentos, onde quer que estejam armazenados. Fornece ferramentas para transformar seus dados, armazenados em banco de dados de séries temporais (TSDB), em gráficos e visualizações inteligentes.

![bg left:50% 100%](imagens/grafana2.png)

---
<style scoped>
  h1 {
    font-size: 48pt;
    text-align: center;
    list-style-type: circle;
    font-weight: 900;
    color: #00FFFF
  }
  img[alt~="center"]{
    display: block;
    margin: 0 auto;
  }
  p{
    text-align: center;
  }
</style>

# Vamos ao Laboratório!
#
#
#
#
#
#
#
#
![bg w:480px](imagens/lab_info.png)

---
<style scoped>
  h1 {
    font-size: 48pt;
    text-align: center;
    list-style-type: circle;
    font-weight: 900;
    color: #00FFFF
  }
  h5{
    font-size: 30px;
    padding: 60px;
    text-align: center;
    margin-bottom: -100px;
  }
  h6{
    font-size: 35pt;
    text-align: center;
    padding: 80px;
    margin-bottom: -200px;
    color: #00FFFF;
  }
</style>

# Vamos nos conectar?


##### ![w:50px](imagens/insta.png) jeovanybsilva
##### ![w:50px](imagens/linedin.png) Jeovany Batista
##### ![w:50px](imagens/telegram.png) jeovanybatista


###### Obrigado!!

---
<style scoped>
  h1 {
    font-size: 35pt;
    text-align: center;
    list-style-type: circle;
    font-weight: 900;
    color: #00FFFF
  }
  p{
    font-size: 21pt;
    padding: 80px;
  }
  a:link {
   color: white;
}

</style>

# Referências Bibliográficas

https://prometheus.io/docs/instrumenting/clientlibs/
https://prometheus.io/docs/introduction/overview/
https://grafana.com/
https://grafana.com/oss/grafana/
https://grafana.com/docs/?pg=oss-graf&plcmt=quick-links
https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack
https://github.com/grafana/helm-charts
