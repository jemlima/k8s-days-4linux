# Observabilidade em clusters K8s com ferramentas Opensource

Este repositório contém manifests para implantar ferramentas de observabilidade em clusters Kubernetes, incluindo o Prometheus, Grafana, Node Exporter, Kube-state-metrics e NGINX. As etapas a seguir descrevem como realizar o deploy desses componentes no cluster.

## Clone do Repositório
Para começar, clone o repositório para obter os manifestos necessários. Execute o seguinte comando:
```
git clone https://gitlab.com/jeovanybatista/k8s-days-4linux.git
```

## Passo 1: Deploy do NGINX

1. Navegue para o diretório contendo os manifestos do NGINX.
   ```
   cd k8s-days-4linux/nginx-controller
   ```

2. Add repositório HELM

```sh
helm repo add nginx-stable https://helm.nginx.com/stable
```

3. Criando o Namespace que será utilizado pelo nginx

```sh
kubectl create namespace ingress-nginx
```
4. Instalando o NGINX Controller

```sh
helm install nginx-control nginx-stable/nginx-ingress -n ingress-nginx
```

## Verificando IP

Após o deploy da aplicação vamos verificar qual IP nosso NGINX pegou com o comando

```sh
export NGINX_INGRESS_IP=$(kubectl -n ingress-nginx get service nginx-control-nginx-ingress -o json | jq -r '.status.loadBalancer.ingress[].ip')
```

Guarde essa informação pois necessitaremos dela!

## Passo 2: Criação do Namespace "monitoring"

1. Execute o seguinte comando para criar o namespace "monitoring":
   ```
   kubectl create namespace monitoring
   ```

## Passo 3: Deploy do Node Exporter

1. Navegue para o diretório contendo os manifestos do Node Exporter.
   ```
   cd ../node-exporter/
   ```

2. Execute o comando a seguir para implantar o Node Exporter no cluster:
   ```
   kubectl apply -f . -n monitoring
   ```

## Passo 4: Deploy do Kube-state-metrics

1. Navegue para o diretório contendo os manifestos do Kube-state-metrics.
   ```
   cd ../kube-state-metrics/deploy-exporter/
   ```

2. Execute o comando a seguir para implantar o Kube-state-metrics no cluster:
   ```
   kubectl apply -f .
   ```

## Passo 5: Deploy do Grafana

1. Navegue para o diretório contendo os manifestos das configmaps do Grafana.
   ```
   cd ../grafana/dashboard
   ```

2. Execute o comando a seguir para implantar as configmaps do Grafana no cluster:
   ```
   kubectl apply -f . -n monitoring
   ```

3. Volte para o diretório raiz do repositório.
   ```
   cd ../
   ```

4. Abra o arquivo `grafana-ingress.yaml` em um editor de texto e edite o valor do campo host substituindo `<NGINX_IP>` pelo valor armazenado na variável `NGINX_INGRESS_IP`.

5. Execute o comando a seguir para implantar o Grafana no cluster:
   ```
   kubectl apply -f grafana.yaml
   ```


## Passo 6: Deploy do Prometheus

1. Navegue para o diretório contendo os manifestos do Prometheus.
   ```
   cd ../prometheus/rules
   ```

2. Execute o comando a seguir para implantar as configmaps das regras do Prometheus no cluster:
   ```
   kubectl apply -f . -n monitoring
   ```

3. Volte para o diretório raiz do repositório.
   ```
   cd ../
   ```

4. Abra o arquivo `prometheus-ingress.yaml` em um editor de texto e edite o valor do campo host substituindo `<NGINX_IP>` pelo valor armazenado na variável `NGINX_IP`.

5. Execute o comando a seguir para implantar o Prometheus no cluster no namespace "monitoring":
   ```
   kubectl apply -f prometheus.yaml -n monitoring
   ```

Após seguir essas etapas, o NGINX, o Node Exporter, o Kube-state-metrics, o Grafana e o Prometheus estarão implantados no seu cluster Kubernetes. Certifique-se de que todas as dependências sejam implantadas corretamente antes de prosseguir com o próximo passo.